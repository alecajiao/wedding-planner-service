create schema weddingPlannerServiceDb
    create table if not exists wedding (
        id serial primary key,
        client_fname varchar(200) not null,
        client_lname varchar(200) not null,
        -- You can also give the constraint a separate name.
        -- This clarifies error messages and allows you to refer to the constraint when you need to change it.
        -- https://www.postgresql.org/docs/8.2/ddl-constraints.html
        budget numeric not null CONSTRAINT positive_price check (budget > 0),
        location varchar(100),
        -- https://www.geeksforgeeks.org/postgresql-date-data-type/
        created date not null default current_date,
        modified date not null default current_date,
        due date not null
    )
    create table if not exists expense (
        id serial primary key,
        -- maybe change to text type later
        reason varchar(200) not null,
        amount numeric not null CONSTRAINT positive_price check (amount > 0),
        -- will be a link
        receipt_img varchar(200),
        created date not null default current_date,
        modified date not null default current_date,
        w_id int not null references wedding(id) ON DELETE CASCADE
    )