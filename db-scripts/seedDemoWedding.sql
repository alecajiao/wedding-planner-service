--Seed weddings
insert into weddingplannerservicedb.wedding (id, client_fname, client_lname, budget, location, created, modified, due) values
    (default, 'Hermione', 'Granger', 10000, '12 Grimmauld Place', default, default, '2021-12-01' ),
    (default, 'Harry', 'Potter', 15000, 'Shell Cottage', default, default, '2022-01-02' ),
    (default, 'Remus', 'Lupin', 9000, 'The leaky Cauldron', default, default, '2021-11-06' );
