import express from "express";
import { Expense } from "./src/entities/expense";
import { Wedding } from "./src/entities/wedding";
import { ExpenseServiceImpl } from "./src/services/service-impls/expense-service-impl";
import { WeddingServiceImpl } from "./src/services/service-impls/wedding-service-impl";
import ExpenseService from "./src/services/service-interfaces/expense-service";
import WeddingService from "./src/services/service-interfaces/wedding-service";
import cors from "cors";

const port = process.env.PORT || 3001;
const app = express();
//application-level middleware
//https://expressjs.com/en/guide/using-middleware.html
//http://expressjs.com/en/api.html#express.json
app.use(express.json()); 
app.use(cors())

// Create an instance of a wedding and expense service implementation
const weddingService:WeddingService = new WeddingServiceImpl();
const expenseService:ExpenseService = new ExpenseServiceImpl();

// Wedding endpoints
app.get("/weddings", async (req, res) => {
    const weddings:Wedding[] = await weddingService.getAllWeddings();
    res.status(201).send(weddings);
});

app.post("/weddings", async (req, res) => {
    try {
        const wedding = await weddingService.createWedding(req.body);
        res.status(201).send(wedding);
    } catch (error) {
        res.status(500).send(error);
    }
});

app.get("/weddings/:id", async (req, res) => {
    try {
        const wedding:Wedding = await weddingService.getWeddingById(Number(req.params.id));
        res.status(200).send(wedding);
    } catch (error) {
        if(error){
            res.status(404).send(error);
        }
    }
});

app.get("/weddings/:id/expenses", async (req, res) => {
    try {
        const expenses:Expense[] = await expenseService.getAllExpensesForWedding(Number(req.params.id));
        res.status(200).send(expenses);
    } catch (error) {
        res.status(404).send(error);
    }
});

app.put("/weddings/:id", async (req, res) => {
    try {
        const wedding:Wedding = await weddingService.getWeddingById(Number(req.params.id));
        let updatedWedding:Wedding = req.body;
        updatedWedding.id = wedding.id;
        updatedWedding = await weddingService.updateWedding(updatedWedding);
        res.status(200).send(updatedWedding);
    } catch (error) {
        res.status(400).send(error);
    }
});

app.delete("/weddings/:id", async (req, res) => {
    try {
        const result = await weddingService.deleteWedding(Number(req.params.id));
        res.status(200).send(result);
    } catch (error) {
        res.status(404).send(error);
    }
});

//expenses endpoints
app.get("/expenses", async (req, res) => {
    const expenses:Expense[] = await expenseService.getAllExpenses();
    res.status(201).send(expenses);
});

app.get("/expenses/:id", async (req, res) => {
    try {
        const expense:Expense = await expenseService.getExpenseById(Number(req.params.id));
        res.status(200).send(expense);
    } catch (error) {
        res.status(404).send(error);
    }
});

app.post("/expenses", async (req, res) => {    
    try {
        const expense:Expense = await expenseService.createExpense(req.body);
        res.status(201).send(expense);
    } catch (error) {
        res.status(500).send(error);
    }
});

app.put("/expenses/:id", async (req, res) => {
    try {
        const expense:Expense = await expenseService.getExpenseById(Number(req.params.id));
        let updatedExpense:Expense = req.body;
        updatedExpense.id = expense.id;
        updatedExpense = await expenseService.updateExpense(updatedExpense);
        res.status(200).send(updatedExpense);
    } catch (error) {
        res.status(400).send(error);
    }
});

app.delete("/expenses/:id", async (req, res) => {
    try {
        const result = await expenseService.deleteExpense(Number(req.params.id));
        res.status(200).send(result);
    } catch (error) {
        res.status(404).send(error);
    }
});

app.listen(port, () => { console.log(`Wedding Planner Service API Application started on port ${port}.`);});

