import { dbClient } from "../src/connection";
import { ExpenseDAOPostgres } from "../src/daos/dao-impls/expense-dao-impl-postgres";
import { WeddingDAOPostgres } from "../src/daos/dao-impls/wedding-dao-impl-postgres";
import { ExpenseDAO } from "../src/daos/dao-interfaces/expense-dao";
import { WeddingDAO } from "../src/daos/dao-interfaces/wedding-dao";
import { Expense } from "../src/entities/expense";
import { Wedding } from "../src/entities/wedding";
import { TestExpense } from "../src/helper-classes/TestExpense";

// Create new expense dao implementation instance
const expenseDAO:ExpenseDAO = new ExpenseDAOPostgres();

//Create a postgres wedding dao 
const weddingDAO:WeddingDAO = new WeddingDAOPostgres();

test("Create expense", async () => {
    // Create new expense instance
   let expense:Expense = await (new TestExpense).getTestExpense();

   // Add new expense to DB
   expense = await expenseDAO.createExpense(expense);

   // Check result
   expect(expense.id).not.toBe(0);   
});

test("Get all expenses", async () => {
    // Get all current expenses to get current expense count
    const expensesBefore:Expense[] = await expenseDAO.getAllExpenses();

    // Create a new expense instance
    const expense:Expense = await (new TestExpense).getTestExpense();

    // Add new expense to DB
    await expenseDAO.createExpense(expense);

    //Get all expenses from db after adding an expense
    const expensesAfter:Expense[] = await expenseDAO.getAllExpenses();

    // Check result
    expect(expensesBefore.length).toBeLessThan(expensesAfter.length);
});

test("Get expense by ID", async () => {
    // Create a new expense instance
    let testExpense:Expense = await (new TestExpense).getTestExpense();
    
    // Add expense to DB
    testExpense = await expenseDAO.createExpense(testExpense);

    // Get newly added expense by its ID
    const retrievedExpense = await expenseDAO.getExpenseById(testExpense.id);

    // Check result
    expect(retrievedExpense.id).toBe(testExpense.id);
});

test("Update expense", async () => {
    // Create a new expense instance
    let testExpense:Expense = await (new TestExpense).getTestExpense();

    // Add expense to DB
    testExpense = await expenseDAO.createExpense(testExpense);

    // Modify expense and update it
    const updatedReason = (await (new TestExpense).getTestExpense()).reason;
    testExpense.reason = updatedReason;
    testExpense = await expenseDAO.updateExpense(testExpense);

    // Check result
    expect(testExpense.reason).toBe(updatedReason);
});

test("Delete expense", async () => {
    // Create a new expense instance
    let testExpense:Expense = await (new TestExpense).getTestExpense();    

    // Add expense to DB
    testExpense = await expenseDAO.createExpense(testExpense);    

    // Delete the newly created expense
    const result: boolean = await expenseDAO.deleteExpense(testExpense.id);
    
    // Check result 
    expect(result).toEqual(true);
});

afterAll(()=>{dbClient.end();});