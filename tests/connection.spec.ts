import { dbClient } from "../src/connection";

test("Should be connected to weddingplannerservicedb", async () => {
    const result = await dbClient.query(
        `SELECT * FROM information_schema.schemata 
        where schema_name = 'weddingplannerservicedb';`)
    
    expect(result.rows[0].schema_name).toBe("weddingplannerservicedb");

});

afterAll(()=>{dbClient.end();});