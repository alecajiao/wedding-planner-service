import { dbClient } from "../src/connection";
import { Wedding } from "../src/entities/wedding";
import { RandomUser } from "../src/helper-classes/RandomUser";
import { TestWedding } from "../src/helper-classes/TestWedding";
import { WeddingServiceImpl } from "../src/services/service-impls/wedding-service-impl";
import WeddingService from "../src/services/service-interfaces/wedding-service";

// Create a wedding service instance
const weddingService:WeddingService = new WeddingServiceImpl();

test("Create wedding", async () => {
    // Create a new wedding instance
    const wedding:Wedding = await TestWedding.getTestWedding();   

    // Add new wedding to DB 
    const result = await weddingService.createWedding(wedding);    

    // Check result
    expect(result.id).not.toBe(0);
});

test("Get all weddings", async () => {
    // Get all current weddings to get current wedding count
    const weddingsBefore:Wedding[] = await weddingService.getAllWeddings();

    // Create a new wedding instance
    const wedding:Wedding = await TestWedding.getTestWedding();

    // Add new wedding to DB
    await weddingService.createWedding(wedding);
    
    // Get all weddings from DB after adding a wedding
    const weddingsAfter:Wedding[] = await weddingService.getAllWeddings();

    // Check result        
    expect(weddingsBefore.length).toBeLessThan(weddingsAfter.length);
});

test("Get wedding by ID", async () => {
    // Create a new wedding instance
    let testWedding:Wedding = await TestWedding.getTestWedding();

    // Add wedding to DB 
    testWedding = await weddingService.createWedding(testWedding);
    
    // Get wedding that was just added by ID
    const retrievedWedding:Wedding = await weddingService.getWeddingById(testWedding.id);

    // Check result 
    expect(retrievedWedding.id).toBe(testWedding.id);
});

test("Update wedding", async () => {
    // Create a new wedding instance
    let testWedding = await TestWedding.getTestWedding();

    // Add wedding to DB 
    testWedding = await weddingService.createWedding(testWedding);

    // Modify wedding and update 
    const randomUser = await RandomUser.getRandomUser();
    testWedding.client_fname = randomUser.fname; 
    testWedding = await weddingService.updateWedding(testWedding);

    // Check result
    expect(testWedding.client_fname).toBe(randomUser.fname);
});

test("Delete wedding", async () => {
    // Create a new wedding instance
    let testWedding:Wedding = await TestWedding.getTestWedding();
    
    // Add wedding to DB 
    testWedding = await weddingService.createWedding(testWedding);

    // Delete the wedding that was just created
    const result:boolean = await weddingService.deleteWedding(testWedding.id);

    // Check result
    expect(result).toEqual(true);
});

afterAll(async () => {
    dbClient.end();
});