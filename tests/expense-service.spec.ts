import { dbClient } from "../src/connection";
import { Expense } from "../src/entities/expense";
import { Wedding } from "../src/entities/wedding";
import { TestExpense } from "../src/helper-classes/TestExpense";
import { TestWedding } from "../src/helper-classes/TestWedding";
import { ExpenseServiceImpl } from "../src/services/service-impls/expense-service-impl";
import { WeddingServiceImpl } from "../src/services/service-impls/wedding-service-impl";
import ExpenseService from "../src/services/service-interfaces/expense-service";
import WeddingService from "../src/services/service-interfaces/wedding-service";

// Create new expense service implementation instance
const expenseService:ExpenseService = new ExpenseServiceImpl();

// Create a wedding service instance
const weddingService:WeddingService = new WeddingServiceImpl();

// Increase the timeout value due to long-running test (Get all expenses for a wedding)
jest.setTimeout(100000);

//Tests
test("Create expense", async () => {
    // Create new expense instance
   const expense:Expense = await (new TestExpense()).getTestExpense();

   // Add new expense to DB
   const result = await expenseService.createExpense(expense);

   // Check result
   expect(result.id).not.toBe(0);   
});

test("Get all expenses", async () => {
    // Get all current expenses to get current expense count
    const expensesBefore:Expense[] = await expenseService.getAllExpenses();

    // Create a new expense instance
    const expense:Expense =  await (new TestExpense()).getTestExpense();

    // Add new expense to DB
    await expenseService.createExpense(expense);

    //Get all expenses from db after adding an expense
    const expensesAfter:Expense[] = await expenseService.getAllExpenses();

    // Check result
    expect(expensesBefore.length).toBeLessThan(expensesAfter.length);
});

test("Get all expenses for a wedding", async () => {
    jest.setTimeout(100000);
    // Create a wedding and add to DB
    let wedding:Wedding = await TestWedding.getTestWedding();
    wedding = await weddingService.createWedding(wedding);

    // Create 5 expenses for newly created wedding and add to DB
    for(let i = 0; i < 5; i++){
        const expense:Expense = await (new TestExpense()).getTestExpense(wedding.id);
        await expenseService.createExpense(expense);
    }

    // Get all the expenses for the newly created wedding 
    const retrievedExpenses = await expenseService.getAllExpensesForWedding(wedding.id);

    // Check result
    expect(retrievedExpenses.length).toBe(5);
});

test("Get expense by ID", async () => {
    // Create a new expense instance
    let testExpense:Expense = await (new TestExpense()).getTestExpense();
    
    // Add expense to DB
    testExpense = await expenseService.createExpense(testExpense);

    // Get newly added expense by its ID
    const retrievedExpense = await expenseService.getExpenseById(testExpense.id);

    // Check result
    expect(retrievedExpense.id).toBe(testExpense.id);
});

test("Update expense", async () => {
    // Create a new expense instance
    let testExpense:Expense = await (new TestExpense()).getTestExpense();

    // Add expense to DB
    testExpense = await expenseService.createExpense(testExpense);

    // Modify expense and update it
    const updatedReason = (await (new TestExpense).getTestExpense()).reason;
    testExpense.reason = updatedReason;
    testExpense = await expenseService.updateExpense(testExpense);

    // Check result
    expect(testExpense.reason).toBe(updatedReason);
});

test("Delete expense", async () => {
    // Create a new expense instance
    let testExpense:Expense = await (new TestExpense).getTestExpense();

    // Add expense to DB
    testExpense = await expenseService.createExpense(testExpense);

    // Delete the newly created expense
    const result:boolean = await expenseService.deleteExpense(testExpense.id);

    // Check result 
    expect(result).toEqual(true);
});

afterAll(()=>{dbClient.end();});