import { dbClient } from "../src/connection";
import { WeddingDAOPostgres } from "../src/daos/dao-impls/wedding-dao-impl-postgres";
import { WeddingDAO } from "../src/daos/dao-interfaces/wedding-dao";
import { Wedding } from "../src/entities/wedding";
import { RandomUser } from "../src/helper-classes/RandomUser";
import { TestWedding } from "../src/helper-classes/TestWedding";

//Create a postgres wedding dao 
const weddingDAO:WeddingDAO = new WeddingDAOPostgres();

test("Create wedding", async () => {
    // Create a new wedding instance
    const wedding:Wedding = await TestWedding.getTestWedding();   

    // Add new wedding to DB 
    const result = await weddingDAO.createWedding(wedding);    

    // Check result
    expect(result.id).not.toBe(0);
});

test("Get all weddings", async () => {
    // Get all current weddings to get current wedding count
    const weddingsBefore:Wedding[] = await weddingDAO.getAllWeddings();

    // Create a new wedding instance
    const wedding:Wedding = await TestWedding.getTestWedding();

    // Add new wedding to DB
    await weddingDAO.createWedding(wedding);
    
    // Get all weddings from DB after adding a wedding
    const weddingsAfter:Wedding[] = await weddingDAO.getAllWeddings();

    // Check result        
    expect(weddingsBefore.length).toBeLessThan(weddingsAfter.length);
});

test("Get wedding by ID", async () => {
    // Create a new wedding instance
    let testWedding:Wedding = await TestWedding.getTestWedding();

    // Add wedding to DB 
    testWedding = await weddingDAO.createWedding(testWedding);
    
    // Get wedding that was just added by ID
    const retrievedWedding:Wedding = await weddingDAO.getWeddingById(testWedding.id);

    // Check result 
    expect(retrievedWedding.id).toBe(testWedding.id);
});

test("Update wedding", async () => {
    // Create a new wedding instance
    let testWedding:Wedding = await TestWedding.getTestWedding();

    // Add wedding to DB 
    testWedding = await weddingDAO.createWedding(testWedding);

    // Modify wedding and update 
    const randomUser = await RandomUser.getRandomUser();
    testWedding.client_fname = randomUser.fname; 
    testWedding = await weddingDAO.updateWedding(testWedding);

    // Check result
    expect(testWedding.client_fname).toBe(randomUser.fname);

});

test("Delete wedding", async () => {
    // Create a new wedding instance
    let testWedding:Wedding = await TestWedding.getTestWedding();
    
    // Add wedding to DB 
    testWedding = await weddingDAO.createWedding(testWedding);

    // Delete the wedding that was just created
    const result:boolean = await weddingDAO.deleteWedding(testWedding.id);
    
    // Check result 
    expect(result).toEqual(true);
});


afterAll(()=>{dbClient.end();});
