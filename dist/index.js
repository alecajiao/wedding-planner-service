"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const expense_service_impl_1 = require("./src/services/service-impls/expense-service-impl");
const wedding_service_impl_1 = require("./src/services/service-impls/wedding-service-impl");
const cors_1 = __importDefault(require("cors"));
const port = 3001;
const app = (0, express_1.default)();
//application-level middleware
//https://expressjs.com/en/guide/using-middleware.html
//http://expressjs.com/en/api.html#express.json
app.use(express_1.default.json());
app.use((0, cors_1.default)());
// Create an instance of a wedding and expense service implementation
const weddingService = new wedding_service_impl_1.WeddingServiceImpl();
const expenseService = new expense_service_impl_1.ExpenseServiceImpl();
// Wedding endpoints
app.get("/weddings", async (req, res) => {
    const weddings = await weddingService.getAllWeddings();
    res.status(201).send(weddings);
});
app.post("/weddings", async (req, res) => {
    try {
        const wedding = await weddingService.createWedding(req.body);
        res.status(201).send(wedding);
    }
    catch (error) {
        res.status(500).send(error);
    }
});
app.get("/weddings/:id", async (req, res) => {
    try {
        const wedding = await weddingService.getWeddingById(Number(req.params.id));
        res.status(200).send(wedding);
    }
    catch (error) {
        if (error) {
            res.status(404).send(error);
        }
    }
});
app.get("/weddings/:id/expenses", async (req, res) => {
    try {
        const expenses = await expenseService.getAllExpensesForWedding(Number(req.params.id));
        res.status(200).send(expenses);
    }
    catch (error) {
        res.status(404).send(error);
    }
});
app.put("/weddings/:id", async (req, res) => {
    try {
        const wedding = await weddingService.getWeddingById(Number(req.params.id));
        let updatedWedding = req.body;
        updatedWedding.id = wedding.id;
        updatedWedding = await weddingService.updateWedding(updatedWedding);
        res.status(200).send(updatedWedding);
    }
    catch (error) {
        res.status(400).send(error);
    }
});
app.delete("/weddings/:id", async (req, res) => {
    try {
        const result = await weddingService.deleteWedding(Number(req.params.id));
        res.status(200).send(result);
    }
    catch (error) {
        res.status(404).send(error);
    }
});
//expenses endpoints
app.get("/expenses", async (req, res) => {
    const expenses = await expenseService.getAllExpenses();
    res.status(201).send(expenses);
});
app.get("/expenses/:id", async (req, res) => {
    try {
        const expense = await expenseService.getExpenseById(Number(req.params.id));
        res.status(200).send(expense);
    }
    catch (error) {
        res.status(404).send(error);
    }
});
app.post("/expenses", async (req, res) => {
    try {
        const expense = await expenseService.createExpense(req.body);
        res.status(201).send(expense);
    }
    catch (error) {
        res.status(500).send(error);
    }
});
app.put("/expenses/:id", async (req, res) => {
    try {
        const expense = await expenseService.getExpenseById(Number(req.params.id));
        let updatedExpense = req.body;
        updatedExpense.id = expense.id;
        updatedExpense = await expenseService.updateExpense(updatedExpense);
        res.status(200).send(updatedExpense);
    }
    catch (error) {
        res.status(400).send(error);
    }
});
app.delete("/expenses/:id", async (req, res) => {
    try {
        const result = await expenseService.deleteExpense(Number(req.params.id));
        res.status(200).send(result);
    }
    catch (error) {
        res.status(404).send(error);
    }
});
app.listen(port, () => { console.log(`Wedding Planner Service API Application started on port ${port}.`); });
