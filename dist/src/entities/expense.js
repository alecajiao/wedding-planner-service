"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Expense = void 0;
class Expense {
    constructor(id, reason, amount, receiptImg, created, modified, wID) {
        this.id = id;
        this.reason = reason;
        this.amount = amount;
        this.receiptImg = receiptImg;
        this.created = created;
        this.modified = modified;
        this.wID = wID;
    }
}
exports.Expense = Expense;
