"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Wedding = void 0;
class Wedding {
    constructor(id, client_fname, client_lname, location, budget, created, modified, due) {
        this.id = id;
        this.client_fname = client_fname;
        this.client_lname = client_lname;
        this.location = location;
        this.budget = budget;
        this.created = created;
        this.modified = modified;
        this.due = due;
    }
}
exports.Wedding = Wedding;
