"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MissingResourceError = void 0;
class MissingResourceError {
    constructor(message) {
        this.description = "This error means a resource cannot be found.";
        this.message = message;
    }
}
exports.MissingResourceError = MissingResourceError;
