"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WeddingDAOPostgres = void 0;
const connection_1 = require("../../connection");
const wedding_1 = require("../../entities/wedding");
const missing_resource_error_1 = require("../../errors/missing-resource-error");
const ISODateHelper_1 = require("../../helper-classes/ISODateHelper");
class WeddingDAOPostgres {
    async createWedding(wedding) {
        const sql = "insert into weddingplannerservicedb.wedding (client_fname, client_lname, budget, location, due) values ($1, $2, $3, $4, $5) returning id";
        const values = [
            wedding.client_fname,
            wedding.client_lname,
            wedding.budget,
            wedding.location,
            wedding.due
        ];
        wedding.id = await (await connection_1.dbClient.query(sql, values)).rows[0].id;
        return wedding;
    }
    async getAllWeddings() {
        const sql = "select * from weddingplannerservicedb.wedding";
        const result = await connection_1.dbClient.query(sql);
        const weddings = [];
        for (const row of result.rows) {
            const wedding = new wedding_1.Wedding(row.id, row.client_fname, row.client_lname, row.location, row.budget, row.created, row.modified, row.due);
            weddings.push(wedding);
        }
        return weddings;
    }
    async getWeddingById(id) {
        const sql = "select * from weddingplannerservicedb.wedding where id = $1";
        const values = [id];
        const result = await connection_1.dbClient.query(sql, values);
        const row = result.rows[0];
        if (result.rowCount === 0) {
            throw new missing_resource_error_1.MissingResourceError(`The wedding with id ${id} does not exist.`);
        }
        const wedding = new wedding_1.Wedding(row.id, row.client_fname, row.client_lname, row.location, row.budget, row.created, row.modified, row.due);
        return wedding;
    }
    async updateWedding(wedding) {
        const today = (new ISODateHelper_1.ISODateHelper).getTodayISODate();
        const sql = "update weddingplannerservicedb.wedding set client_fname = $1, client_lname = $2, budget = $3, location = $4, modified = $5, due = $6 where id = $7";
        const values = [
            wedding.client_fname,
            wedding.client_lname,
            wedding.budget,
            wedding.location,
            wedding.modified = today,
            wedding.due,
            wedding.id
        ];
        // Updating wedding row 
        const result = await connection_1.dbClient.query(sql, values);
        if (result.rowCount === 0) {
            throw new missing_resource_error_1.MissingResourceError(`The wedding with id ${wedding.id} does not exist.`);
        }
        // returning the result of a select query of the row that was edited
        // return (await dbClient.query("select * from weddingplannerservicedb.wedding where id = $1", [wedding.id])).rows[0];
        return this.getWeddingById(wedding.id);
    }
    async deleteWedding(id) {
        const sql = 'delete from weddingplannerservicedb.wedding where id = $1';
        const values = [id];
        const result = await connection_1.dbClient.query(sql, values);
        if (result.rowCount === 0) {
            throw new missing_resource_error_1.MissingResourceError(`The wedding with id ${id} does not exist.`);
        }
        return true;
    }
}
exports.WeddingDAOPostgres = WeddingDAOPostgres;
