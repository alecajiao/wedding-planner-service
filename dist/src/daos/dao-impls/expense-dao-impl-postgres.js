"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExpenseDAOPostgres = void 0;
const connection_1 = require("../../connection");
const expense_1 = require("../../entities/expense");
const missing_resource_error_1 = require("../../errors/missing-resource-error");
const ISODateHelper_1 = require("../../helper-classes/ISODateHelper");
class ExpenseDAOPostgres {
    async createExpense(expense) {
        const sql = "insert into weddingplannerservicedb.expense (reason, amount, receipt_img, w_id) values ($1, $2, $3, $4) returning id";
        const values = [
            expense.reason,
            expense.amount,
            expense.receiptImg,
            expense.wID
        ];
        expense.id = await (await connection_1.dbClient.query(sql, values)).rows[0].id;
        return expense;
    }
    async getAllExpenses() {
        const sql = "select * from weddingplannerservicedb.expense";
        const result = await connection_1.dbClient.query(sql);
        const expenses = [];
        for (const row of result.rows) {
            const expense = new expense_1.Expense(row.id, row.reason, row.amount, row.receipt_img, row.created, row.modified, row.w_id);
            expenses.push(expense);
        }
        return expenses;
    }
    async getExpenseById(id) {
        const sql = "select * from weddingplannerservicedb.expense where id = $1";
        const values = [id];
        const result = await connection_1.dbClient.query(sql, values);
        const row = result.rows[0];
        if (result.rowCount === 0) {
            throw new missing_resource_error_1.MissingResourceError(`The expense with id ${id} does not exist.`);
        }
        const expense = new expense_1.Expense(row.id, row.reason, row.amount, row.receipt_img, row.created, row.modified, row.w_id);
        return expense;
    }
    async updateExpense(expense) {
        const today = (new ISODateHelper_1.ISODateHelper).getTodayISODate();
        const sql = "update weddingplannerservicedb.expense set reason = $1, amount = $2, receipt_img = $3, modified = $4, w_id = $5 where id = $6";
        const values = [
            expense.reason,
            expense.amount,
            expense.receiptImg,
            expense.modified = today,
            expense.wID,
            expense.id
        ];
        // Updating wedding row 
        const result = await connection_1.dbClient.query(sql, values);
        if (result.rowCount === 0) {
            throw new missing_resource_error_1.MissingResourceError(`The expense with id ${expense.id} does not exist.`);
        }
        return this.getExpenseById(expense.id);
    }
    async deleteExpense(id) {
        const sql = "delete from weddingplannerservicedb.expense where id = $1";
        const values = [id];
        const result = await connection_1.dbClient.query(sql, values);
        if (result.rowCount === 0) {
            throw new missing_resource_error_1.MissingResourceError(`The expense with id ${id} does not exist.`);
        }
        return true;
    }
}
exports.ExpenseDAOPostgres = ExpenseDAOPostgres;
