"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.dbClient = void 0;
require('dotenv').config({ path: '/Users/gnz11/Documents/revature/projects/weddingPlannerP1/wedding-planner-service/app.env' });
const pg_1 = require("pg");
exports.dbClient = new pg_1.Client({
    user: process.env.DB_USER,
    password: process.env.DB_PW,
    database: process.env.DB_NAME,
    port: Number(process.env.PORT),
    host: process.env.DB_HOST
});
exports.dbClient.connect();
