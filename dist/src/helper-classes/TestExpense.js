"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TestExpense = void 0;
const axios_1 = __importDefault(require("axios"));
const wedding_dao_impl_postgres_1 = require("../daos/dao-impls/wedding-dao-impl-postgres");
const expense_1 = require("../entities/expense");
const ISODateHelper_1 = require("./ISODateHelper");
class TestExpense {
    constructor() {
        //Create a postgres wedding dao 
        this.weddingDAO = new wedding_dao_impl_postgres_1.WeddingDAOPostgres();
    }
    async getRandomWeddingId() {
        const weddings = await this.weddingDAO.getAllWeddings();
        return weddings[Math.floor(Math.random() * (weddings.length - 1))].id;
    }
    async getTestExpense(weddingID) {
        const dateHelper = new ISODateHelper_1.ISODateHelper();
        const result = await axios_1.default.get("http://hipsum.co/api/?type=hipster-centric&sentences=1");
        const reason = result.data[0];
        //  Create a new wedding instance
        const testExpense = new expense_1.Expense(0, reason, Math.floor(1 + Math.random() * 500), null, '', dateHelper.getTodayISODate(), weddingID || await this.getRandomWeddingId());
        return testExpense;
    }
}
exports.TestExpense = TestExpense;
