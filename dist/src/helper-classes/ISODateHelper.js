"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ISODateHelper = void 0;
class ISODateHelper {
    constructor() {
        this.today = new Date();
    }
    getTodayISODate() {
        let today = this.today.toISOString();
        let i = today.indexOf('T');
        return today.substring(0, i);
    }
    getFutureISODate(date) {
        let futureDate = date.toISOString();
        let i = futureDate.indexOf('T');
        return futureDate.substring(0, i);
    }
}
exports.ISODateHelper = ISODateHelper;
