"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RandomUser = void 0;
const axios_1 = __importDefault(require("axios"));
class RandomUser {
    static async getRandomUser() {
        try {
            const result = await (await axios_1.default.get('https://randomuser.me/api/?nat=us')).data.results[0];
            if (result === undefined) {
                throw new Error("Could not get random user.");
            }
            const user = {
                fname: result.name.first,
                lname: result.name.last,
                location: `${result.location.street.number} ${result.location.street.name}`
            };
            return user;
        }
        catch (error) {
            console.log(error);
        }
    }
}
exports.RandomUser = RandomUser;
