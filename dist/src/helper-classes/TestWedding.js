"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.TestWedding = void 0;
const wedding_1 = require("../entities/wedding");
const ISODateHelper_1 = require("./ISODateHelper");
const RandomUser_1 = require("./RandomUser");
class TestWedding {
    static async getTestWedding() {
        const dateHelper = new ISODateHelper_1.ISODateHelper();
        const randomUser = await RandomUser_1.RandomUser.getRandomUser();
        // Create a new wedding instance
        const testWedding = new wedding_1.Wedding(0, randomUser.fname, randomUser.lname, randomUser.location, Math.floor(1000 + Math.random() * 50000), '', dateHelper.getTodayISODate(), dateHelper.getFutureISODate(new Date(2022, 2, 11)));
        return testWedding;
    }
}
exports.TestWedding = TestWedding;
