"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ExpenseServiceImpl = void 0;
const expense_dao_impl_postgres_1 = require("../../daos/dao-impls/expense-dao-impl-postgres");
class ExpenseServiceImpl {
    constructor() {
        this.expenseDAO = new expense_dao_impl_postgres_1.ExpenseDAOPostgres();
    }
    createExpense(expense) {
        return this.expenseDAO.createExpense(expense);
    }
    getAllExpenses() {
        return this.expenseDAO.getAllExpenses();
    }
    async getAllExpensesForWedding(weddingId) {
        return (await (this.expenseDAO.getAllExpenses())).filter(e => e.wID === weddingId);
    }
    getExpenseById(id) {
        return this.expenseDAO.getExpenseById(id);
    }
    updateExpense(expense) {
        return this.expenseDAO.updateExpense(expense);
    }
    deleteExpense(id) {
        return this.expenseDAO.deleteExpense(id);
    }
}
exports.ExpenseServiceImpl = ExpenseServiceImpl;
