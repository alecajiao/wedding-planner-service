"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.WeddingServiceImpl = void 0;
const wedding_dao_impl_postgres_1 = require("../../daos/dao-impls/wedding-dao-impl-postgres");
class WeddingServiceImpl {
    constructor() {
        this.weddingDAO = new wedding_dao_impl_postgres_1.WeddingDAOPostgres();
    }
    createWedding(wedding) {
        return this.weddingDAO.createWedding(wedding);
    }
    getAllWeddings() {
        return this.weddingDAO.getAllWeddings();
    }
    getWeddingById(id) {
        return this.weddingDAO.getWeddingById(id);
    }
    updateWedding(wedding) {
        return this.weddingDAO.updateWedding(wedding);
    }
    deleteWedding(id) {
        return this.weddingDAO.deleteWedding(id);
    }
}
exports.WeddingServiceImpl = WeddingServiceImpl;
