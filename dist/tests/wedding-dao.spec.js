"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const connection_1 = require("../src/connection");
const wedding_dao_impl_postgres_1 = require("../src/daos/dao-impls/wedding-dao-impl-postgres");
const RandomUser_1 = require("../src/helper-classes/RandomUser");
const TestWedding_1 = require("../src/helper-classes/TestWedding");
//Create a postgres wedding dao 
const weddingDAO = new wedding_dao_impl_postgres_1.WeddingDAOPostgres();
test("Create wedding", async () => {
    // Create a new wedding instance
    const wedding = await TestWedding_1.TestWedding.getTestWedding();
    // Add new wedding to DB 
    const result = await weddingDAO.createWedding(wedding);
    // Check result
    expect(result.id).not.toBe(0);
});
test("Get all weddings", async () => {
    // Get all current weddings to get current wedding count
    const weddingsBefore = await weddingDAO.getAllWeddings();
    // Create a new wedding instance
    const wedding = await TestWedding_1.TestWedding.getTestWedding();
    // Add new wedding to DB
    await weddingDAO.createWedding(wedding);
    // Get all weddings from DB after adding a wedding
    const weddingsAfter = await weddingDAO.getAllWeddings();
    // Check result        
    expect(weddingsBefore.length).toBeLessThan(weddingsAfter.length);
});
test("Get wedding by ID", async () => {
    // Create a new wedding instance
    let testWedding = await TestWedding_1.TestWedding.getTestWedding();
    // Add wedding to DB 
    testWedding = await weddingDAO.createWedding(testWedding);
    // Get wedding that was just added by ID
    const retrievedWedding = await weddingDAO.getWeddingById(testWedding.id);
    // Check result 
    expect(retrievedWedding.id).toBe(testWedding.id);
});
test("Update wedding", async () => {
    // Create a new wedding instance
    let testWedding = await TestWedding_1.TestWedding.getTestWedding();
    // Add wedding to DB 
    testWedding = await weddingDAO.createWedding(testWedding);
    // Modify wedding and update 
    const randomUser = await RandomUser_1.RandomUser.getRandomUser();
    testWedding.client_fname = randomUser.fname;
    testWedding = await weddingDAO.updateWedding(testWedding);
    // Check result
    expect(testWedding.client_fname).toBe(randomUser.fname);
});
test("Delete wedding", async () => {
    // Create a new wedding instance
    let testWedding = await TestWedding_1.TestWedding.getTestWedding();
    // Add wedding to DB 
    testWedding = await weddingDAO.createWedding(testWedding);
    // Delete the wedding that was just created
    const result = await weddingDAO.deleteWedding(testWedding.id);
    // Check result 
    expect(result).toEqual(true);
});
afterAll(() => { connection_1.dbClient.end(); });
