"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const TestWedding_1 = require("../src/helper-classes/TestWedding");
const wedding_service_impl_1 = require("../src/services/service-impls/wedding-service-impl");
// Create a wedding service instance
const weddingService = new wedding_service_impl_1.WeddingServiceImpl();
test("Get all weddings", async () => {
    // Get all current weddings to get current wedding count
    const weddingsBefore = await weddingService.getAllWeddings();
    // Create a new wedding instance
    const wedding = await TestWedding_1.TestWedding.getTestWedding();
    // Add new wedding to DB
    await weddingService.createWedding(wedding);
    // Get all weddings from DB after adding a wedding
    const weddingsAfter = await weddingService.getAllWeddings();
    // Check result        
    expect(weddingsBefore.length).toBeLessThan(weddingsAfter.length);
});
