"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const connection_1 = require("../src/connection");
const RandomUser_1 = require("../src/helper-classes/RandomUser");
const TestWedding_1 = require("../src/helper-classes/TestWedding");
const wedding_service_impl_1 = require("../src/services/service-impls/wedding-service-impl");
// Create a wedding service instance
const weddingService = new wedding_service_impl_1.WeddingServiceImpl();
test("Create wedding", async () => {
    // Create a new wedding instance
    const wedding = await TestWedding_1.TestWedding.getTestWedding();
    // Add new wedding to DB 
    const result = await weddingService.createWedding(wedding);
    // Check result
    expect(result.id).not.toBe(0);
});
test("Get all weddings", async () => {
    // Get all current weddings to get current wedding count
    const weddingsBefore = await weddingService.getAllWeddings();
    // Create a new wedding instance
    const wedding = await TestWedding_1.TestWedding.getTestWedding();
    // Add new wedding to DB
    await weddingService.createWedding(wedding);
    // Get all weddings from DB after adding a wedding
    const weddingsAfter = await weddingService.getAllWeddings();
    // Check result        
    expect(weddingsBefore.length).toBeLessThan(weddingsAfter.length);
});
test("Get wedding by ID", async () => {
    // Create a new wedding instance
    let testWedding = await TestWedding_1.TestWedding.getTestWedding();
    // Add wedding to DB 
    testWedding = await weddingService.createWedding(testWedding);
    // Get wedding that was just added by ID
    const retrievedWedding = await weddingService.getWeddingById(testWedding.id);
    // Check result 
    expect(retrievedWedding.id).toBe(testWedding.id);
});
test("Update wedding", async () => {
    // Create a new wedding instance
    let testWedding = await TestWedding_1.TestWedding.getTestWedding();
    // Add wedding to DB 
    testWedding = await weddingService.createWedding(testWedding);
    // Modify wedding and update 
    const randomUser = await RandomUser_1.RandomUser.getRandomUser();
    testWedding.client_fname = randomUser.fname;
    testWedding = await weddingService.updateWedding(testWedding);
    // Check result
    expect(testWedding.client_fname).toBe(randomUser.fname);
});
test("Delete wedding", async () => {
    // Create a new wedding instance
    let testWedding = await TestWedding_1.TestWedding.getTestWedding();
    // Add wedding to DB 
    testWedding = await weddingService.createWedding(testWedding);
    // Delete the wedding that was just created
    const result = await weddingService.deleteWedding(testWedding.id);
    // Check result
    expect(result).toEqual(true);
});
afterAll(async () => {
    connection_1.dbClient.end();
});
