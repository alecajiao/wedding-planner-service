"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const connection_1 = require("../src/connection");
const expense_dao_impl_postgres_1 = require("../src/daos/dao-impls/expense-dao-impl-postgres");
const wedding_dao_impl_postgres_1 = require("../src/daos/dao-impls/wedding-dao-impl-postgres");
const TestExpense_1 = require("../src/helper-classes/TestExpense");
// Create new expense dao implementation instance
const expenseDAO = new expense_dao_impl_postgres_1.ExpenseDAOPostgres();
//Create a postgres wedding dao 
const weddingDAO = new wedding_dao_impl_postgres_1.WeddingDAOPostgres();
test("Create expense", async () => {
    // Create new expense instance
    let expense = await (new TestExpense_1.TestExpense).getTestExpense();
    // Add new expense to DB
    expense = await expenseDAO.createExpense(expense);
    // Check result
    expect(expense.id).not.toBe(0);
});
test("Get all expenses", async () => {
    // Get all current expenses to get current expense count
    const expensesBefore = await expenseDAO.getAllExpenses();
    // Create a new expense instance
    const expense = await (new TestExpense_1.TestExpense).getTestExpense();
    // Add new expense to DB
    await expenseDAO.createExpense(expense);
    //Get all expenses from db after adding an expense
    const expensesAfter = await expenseDAO.getAllExpenses();
    // Check result
    expect(expensesBefore.length).toBeLessThan(expensesAfter.length);
});
test("Get expense by ID", async () => {
    // Create a new expense instance
    let testExpense = await (new TestExpense_1.TestExpense).getTestExpense();
    // Add expense to DB
    testExpense = await expenseDAO.createExpense(testExpense);
    // Get newly added expense by its ID
    const retrievedExpense = await expenseDAO.getExpenseById(testExpense.id);
    // Check result
    expect(retrievedExpense.id).toBe(testExpense.id);
});
test("Update expense", async () => {
    // Create a new expense instance
    let testExpense = await (new TestExpense_1.TestExpense).getTestExpense();
    // Add expense to DB
    testExpense = await expenseDAO.createExpense(testExpense);
    // Modify expense and update it
    const updatedReason = (await (new TestExpense_1.TestExpense).getTestExpense()).reason;
    testExpense.reason = updatedReason;
    testExpense = await expenseDAO.updateExpense(testExpense);
    // Check result
    expect(testExpense.reason).toBe(updatedReason);
});
test("Delete expense", async () => {
    // Create a new expense instance
    let testExpense = await (new TestExpense_1.TestExpense).getTestExpense();
    // Add expense to DB
    testExpense = await expenseDAO.createExpense(testExpense);
    // Delete the newly created expense
    const result = await expenseDAO.deleteExpense(testExpense.id);
    // Check result 
    expect(result).toEqual(true);
});
afterAll(() => { connection_1.dbClient.end(); });
