"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const connection_1 = require("../src/connection");
test("Should be connected to weddingplannerservicedb", async () => {
    const result = await connection_1.dbClient.query(`SELECT * FROM information_schema.schemata 
        where schema_name = 'weddingplannerservicedb';`);
    expect(result.rows[0].schema_name).toBe("weddingplannerservicedb");
});
afterAll(() => { connection_1.dbClient.end(); });
