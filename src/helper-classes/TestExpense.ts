import axios from "axios";
import { WeddingDAOPostgres } from "../daos/dao-impls/wedding-dao-impl-postgres";
import { WeddingDAO } from "../daos/dao-interfaces/wedding-dao";
import { Expense } from "../entities/expense";
import { Wedding } from "../entities/wedding";
import { ISODateHelper } from "./ISODateHelper";

export class TestExpense {
    private weddingDAO:WeddingDAO;

    constructor(){
    //Create a postgres wedding dao 
     this.weddingDAO = new WeddingDAOPostgres();
    }

   private async getRandomWeddingId():Promise<number> {
        const weddings:Wedding[] = await this.weddingDAO.getAllWeddings();
        return weddings[Math.floor(Math.random() * (weddings.length - 1))].id;
    }

    async getTestExpense(weddingID?:number):Promise<Expense>{
         const dateHelper = new ISODateHelper();
         const result = await axios.get("http://hipsum.co/api/?type=hipster-centric&sentences=1");        
         const reason=  result.data[0];
         
        //  Create a new wedding instance
        const testExpense:Expense = new Expense(
            0,
            reason,
            Math.floor(1 + Math.random() * 500),
            null,
            '',
            dateHelper.getTodayISODate(),
            weddingID || await this.getRandomWeddingId()
        );

         return testExpense;
     }
 }