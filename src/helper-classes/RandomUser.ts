import axios from "axios";

export class RandomUser {
     static async getRandomUser():Promise<any>{
        try {
            const result = await (await axios.get('https://randomuser.me/api/?nat=us')).data.results[0];

            if(result === undefined){
                throw new Error("Could not get random user.");
            }

            const user = {
                fname: result.name.first,
                lname: result.name.last,
                location: `${result.location.street.number} ${result.location.street.name}`
            }

            return user;

        } catch (error) {
            console.log(error); 
        }
    }
}
