import { Wedding } from "../entities/wedding";
import { ISODateHelper } from "./ISODateHelper";
import { RandomUser } from "./RandomUser";

export class TestWedding {
   static async getTestWedding(){
        const dateHelper = new ISODateHelper();
        const randomUser = await RandomUser.getRandomUser();
    
        // Create a new wedding instance
        const testWedding:Wedding = new Wedding(
            0,
            randomUser.fname,
            randomUser.lname,
            randomUser.location,
            Math.floor(1000 + Math.random() * 50000),
            '',
            dateHelper.getTodayISODate(),
            dateHelper.getFutureISODate(new Date(2022, 2, 11))
        );
        return testWedding;
    }
}