export class ISODateHelper {
    private today: Date;

    constructor(){
        this.today = new Date();
    }

    getTodayISODate():string {
        let today = this.today.toISOString();
        let i = today.indexOf('T');
        return today.substring(0, i);
    }

    getFutureISODate(date:Date):string {
        let futureDate = date.toISOString();
        let i = futureDate.indexOf('T');
        return futureDate.substring(0, i);
    }
}
