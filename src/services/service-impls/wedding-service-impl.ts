import { ExpenseDAOPostgres } from "../../daos/dao-impls/expense-dao-impl-postgres";
import { WeddingDAOPostgres } from "../../daos/dao-impls/wedding-dao-impl-postgres";
import { WeddingDAO } from "../../daos/dao-interfaces/wedding-dao";
import { Expense } from "../../entities/expense";
import { Wedding } from "../../entities/wedding";
import WeddingService from "../service-interfaces/wedding-service";

export class WeddingServiceImpl implements WeddingService{
    weddingDAO:WeddingDAO = new WeddingDAOPostgres();

    createWedding(wedding: Wedding): Promise<Wedding> {
        return this.weddingDAO.createWedding(wedding);
    }

    getAllWeddings(): Promise<Wedding[]> {
        return this.weddingDAO.getAllWeddings();
    }

    getWeddingById(id: number): Promise<Wedding> {
        return this.weddingDAO.getWeddingById(id);
    }

    updateWedding(wedding: Wedding): Promise<Wedding> {
        return this.weddingDAO.updateWedding(wedding);
    }
    
    deleteWedding(id: number): Promise<boolean> {
        return this.weddingDAO.deleteWedding(id);
    }
}