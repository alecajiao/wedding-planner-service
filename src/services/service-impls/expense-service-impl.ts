import { ExpenseDAOPostgres } from "../../daos/dao-impls/expense-dao-impl-postgres";
import { ExpenseDAO } from "../../daos/dao-interfaces/expense-dao";
import { Expense } from "../../entities/expense";
import ExpenseService from "../service-interfaces/expense-service";

export class ExpenseServiceImpl implements ExpenseService{
    expenseDAO:ExpenseDAO = new ExpenseDAOPostgres();

    createExpense(expense: Expense): Promise<Expense> {
        return this.expenseDAO.createExpense(expense);
    }

    getAllExpenses(): Promise<Expense[]> {
        return this.expenseDAO.getAllExpenses();
    }

    async getAllExpensesForWedding(weddingId: number): Promise<Expense[]> {
       return (await (this.expenseDAO.getAllExpenses())).filter(e => e.wID === weddingId);       
    }

    getExpenseById(id: number): Promise<Expense> {
        return this.expenseDAO.getExpenseById(id);
    }

    updateExpense(expense: Expense): Promise<Expense> {
        return this.expenseDAO.updateExpense(expense);
    }

    deleteExpense(id: number): Promise<boolean> {
        return this.expenseDAO.deleteExpense(id);
    }
}