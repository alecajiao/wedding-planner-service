import { Wedding } from "../../entities/wedding";

export default interface WeddingService{
    // Basic CRUD operations
    createWedding(wedding:Wedding):Promise<Wedding>;
    getAllWeddings():Promise<Wedding[]>;
    getWeddingById(id:number):Promise<Wedding>;
    updateWedding(wedding:Wedding):Promise<Wedding>;
    deleteWedding(id:number):Promise<boolean>;
}