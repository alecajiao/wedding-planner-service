import { Expense } from "../../entities/expense";

export default interface ExpenseService{
    //Basic CRUD operations
    createExpense(expense:Expense):Promise<Expense>;
    getAllExpenses():Promise<Expense[]>;
    getExpenseById(id:number):Promise<Expense>;
    updateExpense(expense:Expense):Promise<Expense>;
    deleteExpense(id:number):Promise<boolean>;

    //Special GET
    getAllExpensesForWedding(weddingId:number):Promise<Expense[]>;
}