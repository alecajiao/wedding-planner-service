require('dotenv').config({path:'/Users/gnz11/Documents/revature/projects/weddingPlannerP1/wedding-planner-service/app.env'})
import { Client } from "pg"

export const dbClient = new Client({
    user: process.env.DB_USER,
    password: process.env.DB_PW,
    database: process.env.DB_NAME,
    port: Number(process.env.PORT),
    host: process.env.DB_HOST
});

dbClient.connect();