import { dbClient } from "../../connection";
import { Wedding } from "../../entities/wedding";
import { MissingResourceError } from "../../errors/missing-resource-error";
import { ISODateHelper } from "../../helper-classes/ISODateHelper";
import { WeddingDAO } from "../dao-interfaces/wedding-dao";

export class WeddingDAOPostgres implements WeddingDAO{
   async createWedding(wedding: Wedding): Promise<Wedding> {
        const sql:string = "insert into weddingplannerservicedb.wedding (client_fname, client_lname, budget, location, due) values ($1, $2, $3, $4, $5) returning id";
        const values = [
            wedding.client_fname, 
            wedding.client_lname,
            wedding.budget,
            wedding.location,
            wedding.due
        ];

        wedding.id = await (await dbClient.query(sql, values)).rows[0].id;

        return wedding;
    }

    async getAllWeddings(): Promise<Wedding[]> {
        const sql:string = "select * from weddingplannerservicedb.wedding";
        const result = await dbClient.query(sql);
        const weddings:Wedding[] = [];
        for(const row of result.rows){
            const wedding:Wedding = new Wedding(
                row.id,
                row.client_fname,
                row.client_lname,
                row.location,
                row.budget,
                row.created,
                row.modified,
                row.due
            );
            weddings.push(wedding);
        }
        return weddings;
    }

    async getWeddingById(id: number): Promise<Wedding> {
        const sql:string = "select * from weddingplannerservicedb.wedding where id = $1"; 
        const values = [id];
        const result = await dbClient.query(sql, values);
        const row = result.rows[0];

        if(result.rowCount === 0){
            throw new MissingResourceError(`The wedding with id ${id} does not exist.`);
        }
        
        const wedding:Wedding = new Wedding(
            row.id,
            row.client_fname,
            row.client_lname,
            row.location,
            row.budget,
            row.created,
            row.modified,
            row.due
        );

        return wedding;
    }

    async updateWedding(wedding: Wedding): Promise<Wedding> {
        const today = (new ISODateHelper).getTodayISODate();
        const sql:string = "update weddingplannerservicedb.wedding set client_fname = $1, client_lname = $2, budget = $3, location = $4, modified = $5, due = $6 where id = $7";
        const values = [
            wedding.client_fname,
            wedding.client_lname,
            wedding.budget,
            wedding.location,
            wedding.modified = today,
            wedding.due,
            wedding.id
        ];

        // Updating wedding row 
        const result = await dbClient.query(sql, values);

        if(result.rowCount === 0){
            throw new MissingResourceError(`The wedding with id ${wedding.id} does not exist.`);
        }

        // returning the result of a select query of the row that was edited
        // return (await dbClient.query("select * from weddingplannerservicedb.wedding where id = $1", [wedding.id])).rows[0];
        return this.getWeddingById(wedding.id);
    }

    async deleteWedding(id:number): Promise<boolean> {
        const sql:string = 'delete from weddingplannerservicedb.wedding where id = $1';
        const values = [id];
        const result = await dbClient.query(sql, values);
        
        if(result.rowCount === 0){
            throw new MissingResourceError(`The wedding with id ${id} does not exist.`);
        }
        return true;
    }
}