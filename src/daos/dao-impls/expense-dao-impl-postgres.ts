import { dbClient } from "../../connection";
import { Expense } from "../../entities/expense";
import { Wedding } from "../../entities/wedding";
import { MissingResourceError } from "../../errors/missing-resource-error";
import { ISODateHelper } from "../../helper-classes/ISODateHelper";
import { ExpenseDAO } from "../dao-interfaces/expense-dao";

export class ExpenseDAOPostgres implements ExpenseDAO{
    async createExpense(expense: Expense): Promise<Expense> {
        const sql:string = "insert into weddingplannerservicedb.expense (reason, amount, receipt_img, w_id) values ($1, $2, $3, $4) returning id";
        const values = [
            expense.reason,
            expense.amount,
            expense.receiptImg,
            expense.wID
        ];

        expense.id = await (await dbClient.query(sql, values)).rows[0].id;
        return expense;
    }
    async getAllExpenses(): Promise<Expense[]> {
        const sql:string = "select * from weddingplannerservicedb.expense";
        const result = await dbClient.query(sql);
        const expenses:Expense[] = [];
        for(const row of result.rows){
            const expense:Expense = new Expense(
                row.id,
                row.reason,
                row.amount,
                row.receipt_img,
                row.created,
                row.modified,
                row.w_id
            );
            expenses.push(expense);
        }
        return expenses;
    }

    async getExpenseById(id: number): Promise<Expense> {
        const sql:string = "select * from weddingplannerservicedb.expense where id = $1";
        const values = [id];

        const result = await dbClient.query(sql, values);
        const row = result.rows[0];

        if(result.rowCount === 0){
            throw new MissingResourceError(`The expense with id ${id} does not exist.`);
        }

        const expense:Expense = new Expense(
            row.id,
            row.reason,
            row.amount,
            row.receipt_img,
            row.created,
            row.modified,
            row.w_id
        );
        return expense;
    }
    async updateExpense(expense: Expense): Promise<Expense> {
        const today = (new ISODateHelper).getTodayISODate();
        const sql:string = "update weddingplannerservicedb.expense set reason = $1, amount = $2, receipt_img = $3, modified = $4, w_id = $5 where id = $6";
        const values = [
            expense.reason,
            expense.amount,
            expense.receiptImg,
            expense.modified = today,
            expense.wID,
            expense.id
        ];

        // Updating wedding row 
        const result = await dbClient.query(sql, values);

        if(result.rowCount === 0){
            throw new MissingResourceError(`The expense with id ${expense.id} does not exist.`);
        }

        return this.getExpenseById(expense.id);
    }

    async deleteExpense(id: number): Promise<boolean> {
        const sql:string = "delete from weddingplannerservicedb.expense where id = $1";
        const values = [id];
        const result = await dbClient.query(sql, values);        
        
        if(result.rowCount === 0){
            throw new MissingResourceError(`The expense with id ${id} does not exist.`);
        }
        return true;
    }
}