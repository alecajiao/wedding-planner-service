import { Wedding } from "../../entities/wedding";

export interface WeddingDAO {
    //Create
    createWedding(wedding:Wedding):Promise<Wedding>;

    //Read
    getAllWeddings():Promise<Wedding[]>;
    getWeddingById(id:number):Promise<Wedding>;

    //Update
    updateWedding(wedding:Wedding):Promise<Wedding>;

    //Delete
    deleteWedding(id:number):Promise<boolean>;
}