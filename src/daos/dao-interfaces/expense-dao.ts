import { Expense } from "../../entities/expense";

export interface ExpenseDAO{
    //Create
    createExpense(expense:Expense):Promise<Expense>;

    //Read
    getAllExpenses():Promise<Expense[]>;
    getExpenseById(id:number):Promise<Expense>;

    //Update
    updateExpense(expense:Expense):Promise<Expense>;

    //Delete
    deleteExpense(id:number):Promise<boolean>;
}