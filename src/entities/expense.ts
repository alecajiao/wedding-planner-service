export class Expense{
    constructor(
        public id: number,
        public reason: string,
        public amount: number,
        public receiptImg: string,
        public created: string,
        public modified: string,
        public wID: number
    ){}
}