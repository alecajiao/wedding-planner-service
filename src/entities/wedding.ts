export class Wedding {
    constructor(
        public id: number,
        public client_fname: string,
        public client_lname: string,
        public location: string,
        public budget: number,
        public created: string,
        public modified: string,
        public due: string,
    ){}
}