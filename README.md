# Wedding Planner-Service

## Wedding enpoints

## GET
### `http://34.133.53.120:3001/weddings`
### `http://34.133.53.120:3001/weddings/:id`
### `http://34.133.53.120:3001/weddings/:id/expenses`

## POST
### `http://34.133.53.120:3001/weddings/`
### Body Format
`{
    "client_fname": "test",
    "client_lname": "test",
    "budget": 25000,
    "location": "4015 Valwood Pkwy",
    "due": "2022-09-13"
}`

## PUT
### `http://34.133.53.120:3001/weddings/:id`
### Body Format with edited data
`{
    "client_fname": "edited",
    "client_lname": "edited",
    "budget": 25000,
    "location": "4015 Valwood Pkwy",
    "due": "2022-09-13"
}`


## DELETE
### `http://34.133.53.120:3001/weddings/:id`

## Expense endpoints

##GET
### `http://34.133.53.120:3001/expenses`
### `http://34.133.53.120:3001/expenses/:id`

## POST
### `http://34.133.53.120:3001/expenses`
### Body Format
`{
    "reason": "Hot chicken succulents selvage ramps fanny pack shoreditch aesthetic dreamcatcher cray post-ironic.",
    "amount": 500,
    "receiptImg": null,
    "wID": 1
}`

## PUT
### `http://34.133.53.120:3001/expenses/:id`
### Body Format with edited data
`{
    "reason": "Hot chicken succulents selvage ramps fanny pack shoreditch aesthetic dreamcatcher cray post-ironic.",
    "amount": 115,
    "receiptImg": null,
    "wID": 1
}`

## DELETE
### `http://34.133.53.120:3001/expenses/:id`

